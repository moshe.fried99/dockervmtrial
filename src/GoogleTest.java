import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.sikuli.script.ScreenImage;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

public class GoogleTest extends BaseTest {
	private By searchinput=By.xpath("//input[@title='חיפוש']");
	private By searchbutton=By.xpath("//input[@value='חיפוש ב-Google']");
@Test
public void LookForNajdorf() throws IOException, InterruptedException, FindFailed
{
	logger = extent.startTest("lookfornajdorf");

	driver.get("https://www.google.co.il/");
	WebElement search=wait.until(ExpectedConditions.visibilityOfElementLocated(searchinput));
	search.sendKeys("Kaki");
//	System.out.println(search.getText());
//	wait.until(ExpectedConditions.textToBePresentInElement(search, "Najdorf"));
	wait.until(ExpectedConditions.elementToBeClickable(searchbutton)).click();
	Thread.sleep(2000);
	Region r=new Screen().find("/Afarsemon.png");
	r.click();
	Thread.sleep(2000);;

	File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(screenshotFile, new File(System.getProperty("user.dir")+"/FailedScreenShots/123.png"));
    
    
 logger.log(LogStatus.INFO, logger.addScreenCapture(System.getProperty("user.dir")+"/FailedScreenShots/123.png"));
	logger.log(LogStatus.PASS, "Test Case (lookfornajdorf) Status is passed");
}

@Test
public void passTest(){
//extent.startTest("TestCaseName", "Description")
//TestCaseName – Name of the test
//Description – Description of the test
//Starting test
logger = extent.startTest("passTest");
Assert.assertTrue(true);
//To generate the log when the test case is passed
logger.log(LogStatus.PASS, "Test Case Passed is passTest");
}

@Test
public void failTest(){
logger = extent.startTest("failTest");
Assert.assertTrue(false);
logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
}

@Test
public void skipTest(){
logger = extent.startTest("skipTest");
throw new SkipException("Skipping - This is not ready for testing ");
}
}